<?php 
	session_start();
	require "../partials/template.php";
	
	function getBodyContents(){
?>
	<h1 class="text-center py-3">Cart Page</h1>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<th>Item Name:</th>
						<th>Item Price:</th>
						<th>Item Quantity:</th>
						<th>Subtotal</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
							$items = file_get_contents("../assets/lib/products.json");
							$itemsArray = json_decode($items, true);

							$total = 0;
							if(isset($_SESSION["cart"])){
								foreach($_SESSION["cart"] as $itemName => $itemQty){
									foreach($itemsArray as $item){
										if($itemName === $item["name"]){					
											$subtotal = $item["price"]*$itemQty;
											$total += $subtotal;
							?>				
											<tr>
												<td><?php echo $itemName; ?></td>
												<td><?php echo $item["price"]; ?></td>
												<td>
													<form action="../controllers/add-to-cart-process.php" method="POST">
														<input type="hidden" name="itemName" value="<?php echo $itemName; ?>" >
														<input type="hidden" name="fromCartPage" value="fromCartPage">
														<div class="input-group">
															<input type="number" name="itemQty" value="<?php echo $itemQty; ?>" class="form-control">
															<button class="btn btn-success btn-sm">Update</button>
														</div>
													</form>
												</td>
												<td><?php echo $subtotal; ?></td>
												<td>
													<a href="../controllers/remove-from-cart-process.php?name=<?php echo $itemName; ?>" class="btn btn-danger">Remove From Cart</a>
												</td>
											</tr>
							<?php
										}	
									}
								}
							}
						?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Total: <?php echo number_format($total,2,".",","); ?></td>
							<td><a href="../controllers/empty-cart-process.php?" class="btn btn-danger">Empty Cart</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>




<?php
	}
?>