<?php 
	require "../partials/template.php";
	function getBodyContents(){
?>
	<h1 class="text-center py-3">Add Item</h1>
	<div class="col-lg-6 offset-lg-3">
		<form method="POST" enctype="multipart/form-data" action="../controllers/add-item-process.php">
			<div class="form-group">
				<label>Item's Name: </label>
				<input type="text" name="itemName" class="form-control">
			</div>
			<div class="form-group">
				<label>Price: </label>
				<input type="number" name="itemPrice" class="form-control">
			</div>
			<div class="form-group">
				<label>Decription</label>
				<textarea name="itemDescription" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label>Image: </label>
				<input type="file" name="itemImage" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-primary">Add Item</button>
			</div>
			

		</form>

	</div>
	

<?php
	}
?>
