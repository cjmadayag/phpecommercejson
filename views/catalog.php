<?php 
	require "../partials/template.php";
	function getBodyContents(){
?>
<h1 class="text-center py-3">Hi from catalog</h1>
<div class="container">
	<div class="row">
		<?php 
			$products = file_get_contents("../assets/lib/products.json");
			$productsArray = json_decode($products, true);
			foreach ($productsArray as $product){
			?>	
				<div class="col-lg-4 py-2">
					<div class="card">
						<img class="card-img-top" src="../assets/lib/<?php echo $product["image"]; ?>">
					</div>
					<div class="card-body">
						<h5 class="card-title"><?php echo $product["name"]?></h5>
						<p class="card-text">Price: Php <?php echo $product["price"]?></p>
						<p class="card-text">Description: <?php echo $product["description"]?></p>
					</div>
					<div class="card-footer text-center">
						<a href="../controllers/delete-item-process.php?name=<?php echo  $product["name"];?>" class="btn btn-danger">Delete Item</a>
					</div>
					<div class="card-footer">
						<form method="POST" action="../controllers/add-to-cart-process.php">
							<input type="hidden" name="itemName" value="<?php echo $product["name"]?>">
							<input type="number" name="itemQty" value="1" class="form-control">
							<button type="submit" class="btn btn-info btn-block">+ Add to Cart</button>
						</form>
					</div>



				</div>






			<?php
			}
		 ?>
	</div>
</div>



<?php
	}
?>