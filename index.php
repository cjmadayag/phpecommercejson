<!DOCTYPE html>
<html>
<head>
	<title>Store Name</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			  <a class="navbar-brand" href="#">POP!</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>

		  <div class="collapse navbar-collapse" id="navbarColor01">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Item List</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Add Item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="text" placeholder="Search">
		      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
		    </form>
		  </div>
		</nav>
		<div class="d-flex justify-content-center align-items-center flex-column" style="height: 75vh">
			<h1>Welcome to POP!</h1>
			<a href="views/catalog.php" class="btn btn-primary">View List</a>
		</div>
	</header>
	<section>
		<h1 class="text-center p-5">Featured POP! Items</h1>
		<div class="container">
			<div class="row">
				<?php 
					$products = file_get_contents("assets/lib/products.json");
					// var_dump($products);
					$productsArray = json_decode($products,true);
					// var_dump($productsArray);
					$x=0;
					$y=count($productsArray);
					for($i=0; $i<($x+$y); $i++){
						if(isset($productsArray[$i])){

					?>
						<div class="col-lg-4 py-2">
							<div class="card">
								<img src="assets/lib/<?php echo $productsArray[$i]["image"]; ?>" class="card-img-top">
							</div>
							<div class="card-body">
								<h5 class="card-title"><?php echo $productsArray[$i]["name"]; ?></h5>
								<p class="card-text">Price: Php <?php echo $productsArray[$i]["price"] ?></p>
								<p class="card-text">Description: <?php echo $productsArray[$i]["description"] ?></p>
							</div>

						</div>
					<?php
						}
						else{
							$x+=1;
						}
					}
				 	?>
			</div>
		</div>
	</section>
	

</body>
</html>