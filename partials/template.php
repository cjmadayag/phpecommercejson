<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	  <a class="navbar-brand" href="../index.php">POP!</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor01">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="catalog.php">Item List</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="add-item.php">Add Item</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="cart.php">Cart</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">About</a>
	      </li>
	    </ul>
	    <form class="form-inline my-2 my-lg-0">
	      <input class="form-control mr-sm-2" type="text" placeholder="Search">
	      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
	    </form>
	  </div>
	</nav>
	<!-- page content -->
	<?php 
		getBodyContents();

	 ?>



	<!-- footer -->
	<footer class="page-footer font-small bg-dark navbar-dark">
		<div class="footer-copyright text-center py-3">
			2020 Made with Love by: B55			
		</div>
		
	</footer>

</body>
</html>