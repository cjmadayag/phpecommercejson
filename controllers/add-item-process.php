<?php 
	session_start();
	$itemName = $_POST["itemName"];
	$itemPrice = intval($_POST["itemPrice"]);
	$itemDescription = $_POST["itemDescription"];

	// var_dump($_FILES);
	$fileName = $_FILES["itemImage"]["name"];
	$fileSize = $_FILES["itemImage"]["size"];
	$fileTmpName = $_FILES["itemImage"]["tmp_name"];

	// var_dump($fileTmpName);

	$fileType = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

	// var_dump($fileType);

	$hasDetails = false;
	$isImg = false;

	// check if the form is incomplete or empty

	if($itemName="" || $itemPrice>0 || $itemDescription=""){
		$hasDetails = true;
	}

	if($fileType==="jpg" || $fileType==="jpeg" || $fileType==="png" || $fileType==="gif"){
		$isImg = true;
	}

	if($fileSize > 0 && $isImg === true && $hasDetails === true){
		$imgToSave = "images/".$fileName;
		$final_path = "../assets/lib/".$imgToSave;
		move_uploaded_file($fileTmpName, $final_path);

		$newItem = [
			"name"=>$itemName,
			"price"=>$itemPrice,
			"description"=>$itemDescription,
			"image"=>$imgToSave
		];

		$items = file_get_contents("../assets/lib/products.json");
		$items_array = json_decode($items,true);

		array_push($items_array,$newItem);

		// open the file we want to edit
		$toWrite = fopen("../assets/lib/products.json", "w");

		fwrite($toWrite, json_encode($items_array, JSON_PRETTY_PRINT));

		fclose($toWrite);

		header("Location: ../views/catalog.php");


	}
	else{
		echo "Please upload an image";
	}

?>